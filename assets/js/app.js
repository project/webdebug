// Initialise.
document.getElementById('link-error').classList.add('active');
document.getElementById('tab-error').classList.add('tab-visible');


var links = document.querySelectorAll(".tabs a");
links.forEach(function (el) {
  el.addEventListener("click", function () {

    //Hide all tabs.
    var tabs = document.querySelectorAll(".tab-container");
    tabs.forEach(function (tab) {
      tab.classList.remove("tab-visible");
    });

    // Figure out which tab to show.
    var target_id = el.getAttribute("data-target-tab");
    var target = document.getElementById(target_id);

    // Show target tab.
    target.classList.add('tab-visible');

    // Remove all active link classes.
    links.forEach(function (link) {
      link.classList.remove('active');
    });

    // Add active class to clicked link.
    el.classList.add('active');

  });
});
