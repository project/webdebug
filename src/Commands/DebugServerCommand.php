<?php

declare(strict_types=1);

namespace Drupal\webdebug\Commands;

use Drush\Commands\DrushCommands;
use Drupal\Core\Site\Settings;
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use Drupal\webdebug\WebSocket;

/**
 * Starts the Webdebug server.
 */
class DebugServerCommand extends DrushCommands {

  /**
   * A custom Drush command to start a Debug Server.
   *
   * @command webdebug:server
   * @aliases wdbs
   */
  public function server() {
    $port = Settings::get('webdebug_port', '9977');

    $server = IoServer::factory(
      new HttpServer(
        new WsServer(
          new WebSocket()
        )
      ),
      $port
    );

    try {
      $this->output()->writeln('');
      $this->output()->writeln("[DBS] Debug Server listening on localhost:{$port}]");
      $this->output()->writeln("Type CTRL+C to exit.");
      $this->output()->writeln('');
      $server->run();
    }
    catch (\Exception $e) {
      $this->output()->writeln("Error starting server: {$e->getMessage()}");
    }

  }

}
