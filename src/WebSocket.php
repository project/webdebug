<?php

declare(strict_types=1);

namespace Drupal\webdebug;

use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;

/**
 * Websocket class.
 *
 * Keeps track of the attached clients.
 * Sends messages in all available formats to clients.
 * Clients decide which format to use to display the message.
 */
class WebSocket implements MessageComponentInterface {

  /**
   * The list of connected clients.
   *
   * @var \SplObjectStorage
   */
  public $clients;

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->clients = new \SplObjectStorage();
  }

  /**
   * {@inheritdoc}
   */
  public function onOpen(ConnectionInterface $conn): void {
    // Store the new connection in $this->clients.
    $this->clients->attach($conn);

    echo "New connection! ({$conn->resourceId})\n";
  }

  /**
   * {@inheritdoc}
   */
  public function onMessage(ConnectionInterface $from, $msg): void {

    foreach ($this->clients as $client) {
      if ($from->resourceId == $client->resourceId) {
        continue;
      }

      $output = json_decode($msg, TRUE);

      if (isset($output['cli'])) {
        \fwrite(\STDOUT, $output['cli']);
      }

      if (isset($output['html'])) {
        $client->send($output['html']);
      }

    }
  }

  /**
   * {@inheritdoc}
   */
  public function onClose(ConnectionInterface $conn) : void {
    $this->clients->detach($conn);
  }

  /**
   * {@inheritdoc}
   */
  public function onError(ConnectionInterface $conn, \Exception $e) : void {
    echo "Error: {$e->getMessage()}";
    $conn->close();
  }

}
