<?php

declare(strict_types=1);

namespace Drupal\webdebug\Sender;

use Drupal\Core\Site\Settings;
use Ratchet\Client as WSClient;
use GuzzleHttp\Client as HttpClient;

/**
 * Class WebSocketSender. Connects to a websocket and sends messages.
 */
class WebSocketSender {

  /**
   * Creates a Websocket connection and sends a message.
   *
   * @param string $output
   *   The output to send to the socket.
   */
  public static function send(string $output = ''): array {

    if (empty($output)) {
      $content = (string) \Drupal::request()->getContent();
      $output = \json_decode($content);

      if (empty($output) || $output == FALSE) {
        throw new \Exception("Output to send to WebSocket cannot be empty.");
      }
    }

    if (!self::isJson($output)) {
      $output = \json_encode($output);
    }

    $port = Settings::get('webdebug_port', '9977');
    $url = "ws://localhost:{$port}";
    WSClient\connect($url)->then(function ($conn) use ($output) {
      $conn->send($output);
      $conn->close();
    }, function ($e) {
      die("Could not connect: {$e->getMessage()}");
    });
    return [];
  }

  /**
   * Sends the output via an http call to WebSocketSender::send().
   *
   * When execution gets locked (in the case of active breakpoints)
   * it seems that a straight call to send() gets blocked, whereas
   * calling send() via an http request works around this issue.
   *
   * @todo figure out what's really happening (blocked thread?) and
   * decide if this is an acceptable workaround.
   */
  public static function httpSend(string $output) : void {

    if (!self::isJson($output)) {
      $output = json_encode($output);
    }

    $base_uri = \Drupal::request()->getSchemeAndHttpHost();
    $endpoint = '/api/webdebug/message/send';
    $client = new HttpClient();
    $client->post($base_uri . $endpoint, [
      'json' => $output,
    ]);
  }

  /**
   * Checks if a given string is valid JSON.
   *
   * @param string $string
   *   The string to check.
   *
   * @return bool
   *   True if given string is valid JSON.
   */
  private static function isJson(string $string): bool {
    $data = json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
  }

}
