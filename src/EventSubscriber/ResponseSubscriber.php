<?php

namespace Drupal\webdebug\EventSubscriber;

use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Response subscriber.
 */
class ResponseSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::FINISH_REQUEST][] = ['onRespond', 100];
    return $events;
  }

  /**
   * WIP!
   *
   * @param \Symfony\Component\HttpKernel\Event\FilterResponseEvent $event
   *   The event to process.
   */
  public function onRespond(FilterResponseEvent $event) {
    // Work in progress!
    $response = $event->getResponse();
    if ($response->getStatusCode() == '500') {
      die('500!');
    }
  }

}
