<?php

namespace Drupal\webdebug\EventSubscriber;

use Drupal\Core\EventSubscriber\HttpExceptionSubscriberBase;
use Drupal\Core\Utility\Error;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Twig\Environment;

/**
 * Catch-all Http Exception subscriber.
 */
class ExceptionSubscriber extends HttpExceptionSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::EXCEPTION][] = ['onException', 200];
    return $events;
  }

  /**
   * {@inheritdoc}
   */
  protected function getHandledFormats(): array {
    return [
      'html',
    ];
  }

  /**
   * Handles errors for this subscriber.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent $event
   *   The event to process.
   *
   * @todo Should we be using core's Error class here?
   */
  public function onException(GetResponseForExceptionEvent $event) {
    $exception = $event->getException();
    $error = Error::decodeException($exception);

    $params = [
      'type' => $error['%type'],
      'trace' => $exception->getTraceAsString(),
      'message' => $exception->getMessage(),
      'line' => $exception->getLine(),
      'file' => $exception->getFile(),
      'error' => print_r($error, TRUE),
    ];

    /** @var \Twig\Environment $environment */
    $environment = new Environment();
    $content = $environment
      ->loadTemplate('webdebug-error-page.html.twig')
      ->render($params);

    $response = new Response($content, 500);
    $event->setResponse($response);
  }

}
