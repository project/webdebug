<?php

namespace Drupal\webdebug\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

/**
 * WIP Subscriber.
 */
class ErrorSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::EXCEPTION][] = ['onException', 200];
    return $events;
  }

  /**
   * WIP.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent $event
   *   The event that triggered the exception.
   */
  public function onExeption(GetResponseForExceptionEvent $event) {
    // $content = 'CUSTOM ERROR';
    // $response = new Response($content, 500, ['Content-Type' => 'html']);
    // $exception = $event->getException();
    // $error = Error::decodeException($exception);
    // If ($exception instanceof HttpExceptionInterface) {
    //   $response->setStatusCode($exception->getStatusCode());
    //   $response->headers->add($exception->getHeaders());
    // }
    // else {
    //   $response->setStatusCode(
    //     Response::HTTP_INTERNAL_SERVER_ERROR, 'BOOH!'
    //   );
    // }.
    // $event->setResponse($response);
  }

}
