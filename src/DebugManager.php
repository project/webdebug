<?php

declare(strict_types=1);

namespace Drupal\webdebug;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\webdebug\Renderer\CliRenderer;
use Drupal\webdebug\Renderer\HtmlRenderer;
use Drupal\webdebug\Sender\WebSocketSender;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the DebugManager service.
 *
 * This is the user-facing API for debugging activities:
 * message(), dump(), breakpoint().
 *
 * @package Drupal\webdebug\Services
 */
final class DebugManager implements ContainerInjectionInterface {
  /**
   * The sender responsible for sending messages.
   *
   * @var \Drupal\webdebug\Sender\WebSocketSender
   */
  private $webSocketSender;

  /**
   * The path to the app root.
   *
   * @var string
   */
  private $appRoot;

  /**
   * The cache to use.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  private $cache;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cache.default'),
      $container->get('app.root')
    );
  }

  /**
   * Constructs the object.
   *
   * @see \Drupal\webdebug\Sender\WebSocketSender
   */
  public function __construct(CacheBackendInterface $cache, $appRoot) {
    $this->webSocketSender = new WebSocketSender();
    $this->cache = $cache;
    $this->appRoot = $appRoot;
  }

  /**
   * Dumps a variable and sends the output to webdebug clients.
   *
   * @param mixed $var
   *   The variable to dump.
   * @param string $label
   *   The optional label/message to send along with the variable.
   */
  public function dump($var, string $label = '') : void {
    $packet = new Packet($label);
    $packet->type = Packet::PACKET_TYPE_DUMP;
    $this->setFileAndLine($packet);

    $packet->grips[] = new Grip($var);
    $this->sendOutput($packet);
  }

  /**
   * Sends a text string to webdebug clients.
   *
   * @param string $label
   *   The label/message to send.
   */
  public function message(string $label) : void {
    $packet = new Packet($label);
    $packet->type = Packet::PACKET_TYPE_MESSAGE;
    $this->setFileAndLine($packet);
    $this->sendOutput($packet);
  }

  /**
   * Halts all code execution and sends notification to webdebug clients.
   *
   * @param string $label
   *   The label/message to send. Defaults to the breakpoint's internal id.
   *
   * @return bool
   *   Returns true after execution has resumed.
   *   @todo this smells bad. Refactor.
   *   @todo this is an internal method. Move it elsewhere?
   */
  public function breakpoint(string $label = '') : bool {
    $packet = new Packet($label);
    $packet->type = Packet::PACKET_TYPE_BREAKPOINT;
    $this->setFileAndLine($packet);

    // @todo gather env info and stuff it into grips.
    $output = $this->prepareOutput($packet);

    // Use our webservice endpoint to send the breakpoint message
    // instead of a direct call to ::send()
    // to work around an issue where such a call can't be completed
    // because of the do/while loop that follows.
    // Unsure why this is happening but I expect some kind of resource
    // lock due to the single threaded nature of php.
    // @todo get to the bottom of this.
    $this->webSocketSender->httpSend(json_encode($output));

    // Save breakpoint identifier + status ('1') to cache.
    // Clicking the 'remove breakpoint' link in the web client
    // will remove the breakpoint from cache, which will in turn
    // end the do/while loop so normal execution can continue.
    // Cache for 1hr.
    $cid = "webdebug.{$packet->id}";
    // \Drupal::cache()->set($cid, '1', time() + 3600);
    $this->cache->set($cid, '1', time() + 3600);

    do {
      sleep(1);
    } while ($this->breakpointIsActive($packet->id));

    return TRUE;
  }

  /**
   * Removes a given breakpoint. For internal use only.
   *
   * @param int $breakpoint_id
   *   The internal identifier of the breakpoint.
   *
   * @return array
   *   Returns an empty array.
   *   @todo this smells bad. Refactor.
   *   @todo this is an internal method. Move it elsewhere?
   */
  public function removeBreakpoint(int $breakpoint_id) : array {
    $cid = "webdebug.{$breakpoint_id}";
    $this->cache->delete($cid);
    $this->message("Breakpoint {$breakpoint_id} removed.");
    return [];
  }

  /**
   * Checks if a given breakpoint is still active.
   *
   * @param int $breakpoint_id
   *   The internal identifier of the breakpoint.
   *
   * @return bool
   *   Returns TRUE if active, FALSE if not active.
   */
  public function breakpointIsActive(int $breakpoint_id) {
    $cid = "webdebug.{$breakpoint_id}";
    $status = ($this->cache->get($cid));
    return $status;
  }

  /**
   * Determines in which file and line number the debug statement took place.
   *
   * @param \Drupal\webdebug\Packet $packet
   *   The packet to update with the file and line number information.
   *
   * @todo Use or incorporate stuff from Drupal\Core\Utility\Error?
   */
  private function setFileAndLine(Packet &$packet): void {
    $trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
    // Grab the stack frame where the debug statement originated.
    // The very last stack frame points here (setFileAndLine), so ignore it.
    if (isset($trace[1])) {
      $packet->filePath = str_replace($this->appRoot, '', $trace[1]['file']);
      $packet->line = $trace[1]['line'];
    }
    else {
      $packet->filePath = '[unknown]';
      $packet->line = 0;
    }

  }

  /**
   * Prepares the output for sending.
   *
   * @param \Drupal\webdebug\Packet $packet
   *   The packet to prepare.
   *
   * @todo refactor output types into plugins.
   */
  private function prepareOutput(Packet $packet): Array {
    $output = [];
    $output['cli'] = CliRenderer::render($packet);
    $output['html'] = HtmlRenderer::render($packet);

    return $output;
  }

  /**
   * Sends the output.
   *
   * @param \Drupal\webdebug\Packet $packet
   *   The packet to send.
   */
  private function sendOutput(Packet $packet) {
    $output = $this->prepareOutput($packet);
    $this->webSocketSender->send(json_encode($output));
  }

}
