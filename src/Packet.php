<?php

declare(strict_types=1);

namespace Drupal\webdebug;

/**
 * Represents a variable + all its contextual information.
 *
 * A value object that encapsulates the grip (variable from the debuggee),
 * timestamp, and stack trace.
 */
class Packet {

  const PACKET_TYPE_DUMP = 'dump';
  const PACKET_TYPE_BREAKPOINT = 'breakpoint';
  const PACKET_TYPE_MESSAGE = 'message';

  /**
   * The packet's internal id.
   *
   * @var int
   */
  public $id;

  /**
   * The label that's sent along with the packet.
   *
   * @var string
   */
  public $label;

  /**
   * The path to the file the debug call originated.
   *
   * @var string
   */
  public $filePath;

  /**
   * The line number where the debug call originated.
   *
   * @var int
   */
  public $line;

  /**
   * The type of packet being sent.
   *
   * Options: (message | dump | breakpoint).
   *
   * @var string
   */
  public $type;


  /**
   * The grips that are the payload of this packet.
   *
   * @var \Drupal\webdebug\Grip[]
   *
   * @todo Should this still be an array? 1 package should only contain 1 grip.
   */
  public $grips;

  /**
   * The packet's creation timestamp.
   *
   * @var int
   */
  public $timestamp;

  /**
   * Constructs a packet object.
   *
   * @param string $label
   *   The label to show alongside the packet's payload.
   */
  public function __construct(string $label) {
    $this->label = $label;
    $this->id = \hrtime(TRUE);
    $this->timestamp = \time();
    $this->grips = [];
  }

}
