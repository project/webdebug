<?php

declare(strict_types=1);

namespace Drupal\webdebug;

use Symfony\Component\VarDumper\Cloner\VarCloner;

/**
 * Class Grip.
 *
 * A single grip represents a JSON value that refers to a specific PHP value
 * in the debuggee. Grips appear anywhere an arbitrary value from the debuggee
 * needs to be sent to the debug client.
 *
 * @see Mozilla's Remote Debug Protocol
 * (https://firefox-source-docs.mozilla.org/devtools/backend/protocol.html).
 */
class Grip {

  /**
   * The type of symbol being reported upon.
   *
   * Any valid php symbol type, as returned by \gettype(), such as
   * string, int, array, object, callable, etc.
   *
   * @var string
   */
  public $type;

  /**
   * X.
   *
   * @var string
   *
   * @todo Provide proper comment.
   */
  public $name;

  /**
   * The variable's full namespace (if applicable).
   *
   * @var string
   */
  public $namespaceName;

  /**
   * The variable's class name (if applicable).
   *
   * @var string
   */
  public $shortName;

  /**
   * X.
   *
   * @var \Symfony\Component\VarDumper\Cloner\Data
   */
  public $varData;

  /**
   * X.
   *
   * @var string
   *
   * @see \Symfony\Component\VarDumper\Cloner\VarCloner.
   *
   * @todo Provide proper comment.
   */
  public $var;

  /**
   * Constructs a Grip object.
   *
   * A Grip represents a single variable being inspected and sent
   * from the debugee to the client(s).
   *
   * @param mixed $var
   *   The variable being inspected.
   *
   * @todo Populate name, namespace, shortname.
   */
  public function __construct($var) {
    $this->var = $var;
    $this->type = \gettype($this->var);
    $this->varData = (new VarCloner)->cloneVar($var);
  }

}
