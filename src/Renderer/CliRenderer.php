<?php

declare(strict_types=1);

namespace Drupal\webdebug\Renderer;

use Drupal\webdebug\Packet;
use Symfony\Component\VarDumper\Dumper\AbstractDumper;
use Symfony\Component\VarDumper\Dumper\CliDumper;

/**
 * Class CliRenderer. Renders output for a provided Packet in text format.
 */
class CliRenderer extends Renderer {

  /**
   * {@inheritdoc}
   */
  public static function render(Packet $packet): string {
    $dumper = self::getDumper();

    $time = date('H:i:s', $packet->timestamp);

    $output = '';
    $output .= PHP_EOL . "---------------" . PHP_EOL;
    $output .= "{$time} | {$packet->id} | {$packet->label}" . PHP_EOL;
    $output .= ">> {$packet->filePath} (line {$packet->line})" . PHP_EOL;
    $output .= PHP_EOL;

    /** @var \Drupal\webdebug\Grip $grip */
    foreach ($packet->grips as $grip) {
      $output .= $dumper->dump($grip->varData, TRUE);
    }
    $output .= PHP_EOL;

    return $output;
  }

  /**
   * Returns an instance of the dumper class to use.
   *
   * @return \Symfony\Component\VarDumper\Dumper\CliDumper
   *   The CliDumper to use.
   */
  protected static function getDumper(): CliDumper {
    return new CliDumper(NULL, NULL, AbstractDumper::DUMP_LIGHT_ARRAY);
  }

}
