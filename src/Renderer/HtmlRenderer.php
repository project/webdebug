<?php

declare(strict_types=1);

namespace Drupal\webdebug\Renderer;

use Drupal\introspector\Util;
use Drupal\Core\Link;
use Drupal\Core\Site\Settings;
use Drupal\webdebug\Packet;
use Symfony\Component\VarDumper\Dumper\AbstractDumper;
use Symfony\Component\VarDumper\Dumper\HtmlDumper;

/**
 * Class HtmlRenderer. Renders output for a provided Packet in html format.
 */
class HtmlRenderer extends Renderer {

  /**
   * {@inheritdoc}
   */
  public static function render(Packet $packet): string {
    $dumper = self::getDumper();

    $filename_parts = \explode(DIRECTORY_SEPARATOR, $packet->filePath);

    // Prepare twig parameters.
    $params = [];
    $params['id'] = $packet->id;
    $params['type'] = $packet->type;
    $params['time'] = \date('H:i:s', $packet->timestamp);
    $params['label'] = $packet->label;
    $params['path_to_file'] = $packet->filePath;
    $params['filename'] = \end($filename_parts);
    $params['line'] = $packet->line;
    $params['vars'] = [];

    /** @var \Drupal\webdebug\Grip $grip */
    foreach ($packet->grips as $grip) {
      $params['vars'][] = [
        'type' => $grip->type,
        'dump' => $dumper->dump($grip->varData, TRUE),
        'info' => self::getVarInfo($grip->var),
      ];
    }

    $environment = \Drupal::service('twig');
    $environment->enableDebug();
    $output = $environment
      ->loadTemplate("webdebug-packet--{$packet->type}.html.twig")
      ->render($params);

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  protected static function getDumper() : HtmlDumper {
    // Prepare html dumper.
    $dumper = new HtmlDumper(NULL, NULL, AbstractDumper::DUMP_LIGHT_ARRAY);
    $theme = Settings::get('webdebug_code_theme', 'dark');
    if (!in_array($theme, ['light', 'dark'])) {
      $theme = 'dark';
    }
    $dumper->setTheme($theme);
    return $dumper;
  }

  /**
   * Returns class name as plain text or link.
   *
   *  When the Introspector module is present, this function returns a link to
   *  view the variable's class' source code; returns the class name as plain
   *  text otherwise.
   *
   * @param mixed $var
   *   The variable about which class info is to be obtained.
   *
   * @return string
   *   Class name as plain text, or a fully rendered HTML link.
   */
  protected static function getVarInfo($var) : string {
    $info = '';

    if (is_object($var)) {
      $class_name = get_class($var);

      // Turn class_name into link to Introspector class source page.
      if (\Drupal::service('module_handler')->moduleExists('introspector')) {
        // Retrieve name of file that defines this class.
        $rc = new \ReflectionClass($class_name);
        // Will yield FALSE if the class is defined by PHP or an extension.
        if ($rc->getFileName()) {
          $class_name_processed = Util::encodePath($class_name);
          $class_info_link = Link::createFromRoute(
            $class_name,
            'introspector.class_info_source',
            ['class_name' => $class_name_processed],
            ['attributes' => ['target' => '_blank']]
          )->toRenderable();
          $info = \Drupal::service('renderer')->render($class_info_link)->__toString();
        }
        else {
          $info = $class_name;
        }
      }
      else {
        $info = $class_name;
      }
    }
    return $info;
  }

}
