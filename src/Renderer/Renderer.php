<?php

declare(strict_types=1);

namespace Drupal\webdebug\Renderer;

use Drupal\webdebug\Packet;
use Symfony\Component\VarDumper\Cloner\DumperInterface;

/**
 * Abstract class Renderer. Facilitates the rendering of a Packet instance.
 */
abstract class Renderer {

  /**
   * Renders output for the provided Packet instance.
   *
   * @param \Drupal\webdebug\Packet $packet
   *   The packet to render.
   *
   * @return string
   *   The final output to be returned to the end user.
   */
  abstract public static function render(Packet $packet) : string;

  /**
   * Returns an instance of the dumper class to use.
   *
   * @return \Symfony\Component\VarDumper\Cloner\DumperInterface
   *   The dumper class to use.
   */
  abstract protected static function getDumper() : DumperInterface;

}
