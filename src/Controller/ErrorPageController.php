<?php

declare(strict_types=1);

namespace Drupal\webdebug\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Template\TwigEnvironment;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\AbstractDumper;
use Symfony\Component\VarDumper\Dumper\HtmlDumper;


use Drupal\Core\Utility\Error;
use Drupal\Core\Render\Markup;

/**
 * Class ErrorPageController. Provides a more useful error page.
 */
class ErrorPageController extends ControllerBase {

  /**
   * The Module Handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * The Twig Environment.
   *
   * @var \Drupal\Core\Template\TwigEnvironment
   */
  protected $twig;

  /**
   * The Request Stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  protected $appRoot;


  protected $error;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('twig'),
      $container->get('request_stack'),
      $container->get('app_root')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(ModuleHandler $moduleHandler, TwigEnvironment $twig, RequestStack $requestStack, $appRoot) {
    
    $this->moduleHandler = $moduleHandler;
    $this->twig = $twig;
    $this->requestStack = $requestStack;
    $this->appRoot = $appRoot;
  }


  /**
   * Renders the error page.
   *
   * Uses twig and tries to rely as little as possible on Drupal internals
   * to avoid being impacted by whatever is going wrong that triggered
   * this error page in the first place.
   *
   * In other words: render the error page as fast as possible, in as
   * robust a way as possible.
   */
  public function build(array $error, array $options = [], bool $return = FALSE) {
    $this->error = $error;

    // Set defaults.
    $options['status_code'] = $options['status_code'] ?? 500;
    $options['status_message'] = $options['status_message'] ?? '500 Service unavailable';

    // Build help.
    $help = [];
    foreach ($this->help() as $fragment => $explanations) {
      if (strpos($error['@message'], $fragment) !== FALSE) {
        $help = array_merge($help, $explanations);
      }
    }

    // Build file and source info.
    // $file = $this->getSourceSnippet();

    // Build Request info.
    $cloner = new VarCloner();
    $dumper = new HtmlDumper();

    $params = [];
    $request = FALSE;

    if ($request = $this->requestStack->getCurrentRequest()) {
      $headers = $dumper->dump($cloner->cloneVar($request->headers), TRUE);
      $params['request_headers'] = $headers;
    }

    $module_path = $this->moduleHandler
      ->getModule('webdebug')
      ->getPath();

    $backtrace = [];
    // Ensure file paths start at App root.
    if (is_array($error['backtrace'])) {
      foreach ($error['backtrace'] as $key => $item) {
        if (isset($item['file'])) {
          $item['file'] = str_replace(
            $this->appRoot, '', $item['file']
          );
        }
        $backtrace[$key] = $item;
      }
    }
    //dump($backtrace);
    // $trace_dump = $dumper->dump(
    //   (new VarCloner)->cloneVar($backtrace),
    //   TRUE
    // );

    $params = [
      'type' => $error['%type'],
      'trace' => $backtrace,
      'message' => $error['@message'],
      'line' => $error['%line'],
      'file' => $error['%file'],
      'file' => $this->getSourceSnippet(),
      'err' => $error,
      'help' => $help,
      'module_path' => $module_path,
    ];
    $output = $this->twig
      ->loadTemplate('webdebug-error-page.html.twig')
      ->render($params);

    if ($return) {
      return $output;
    }
    else {
      $response = new Response();
      $response
        ->setStatusCode($options['status_code'], $options['status_message'])
        ->setContent($output)
        ->send();
      exit;
    }
  }

  /**
   * Returns an array of potentially helpful explanatations and solutions.
   *
   * @todo Fetch this info as json from an external source.
   */
  private function help(): Array {
    $fragments = [
      "expecting ';'" => [
        [
          'explanation' => "A semi-colon (';') may be missing at the end of the 
            line of code that precedes the indicated line.",
          'solution' => "Ensure that the preceding statement ends with a 
            semi-colon.",
        ],
      ],
    ];

    return $fragments;
  }

  /**
   * Fetches the source code surrounding an indicated file + line number.
   *
   * @return array
   *   Array with backtrace, file, and line info.
   * @see _drupal_log_error()
   */
  protected function getSourceSnippet(): array {
    $file_parts = explode('/', $this->error['%file']);
    $filename = array_pop($file_parts);
    $path_to_file = implode('/', $file_parts);

    $lines_to_show = 5;
    $source_lines = [];
    $file = [];
    if ($f = fopen($this->error['%file'], "r")) {
      $i = 0;
      while (!feof($f)) {
        $l = fgets($f);
        $pre_lines = $this->error['%line'] - $lines_to_show;
        $post_lines = $this->error['%line'] + $lines_to_show;
        if ($i >= $pre_lines && $i <= $post_lines) {
          $source_lines[] = $l;
        }
        $i++;
      }
      fclose($f);

      $file = [
        'filename' => $filename,
        'path_to_file' => $path_to_file,
        'source_lines' => $source_lines,
      ];
    }
    return $file;
  }

}
