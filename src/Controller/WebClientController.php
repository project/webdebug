<?php

declare(strict_types=1);

namespace Drupal\webdebug\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Site\Settings;
use Drupal\Core\Template\TwigEnvironment;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class WebClientController. Provides a web-based webdebug client.
 */
final class WebClientController extends ControllerBase {

  /**
   * The port that the webdebug server and client(s) listen to.
   *
   * Uses $settings['webdebug_port'] if specified; defaults to 9977.
   *
   * @var string
   *
   * @see README.md for info on mapping ports when using virtual machines
   * or container-based environments like Docker.
   */
  public $port;

  /**
   * The Twig Environment.
   *
   * @var \Drupal\Core\Template\TwigEnvironment
   */
  protected $twig;

  /**
   * The Module Handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('twig'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(TwigEnvironment $twig, ModuleHandler $moduleHandler) {
    $this->port = Settings::get('webdebug_port', '9977');
    $this->twig = $twig;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * Builds and renders the error page.
   *
   * This page replaces Drupal's default (nearly) blank error page.
   */
  public function launch() {
    // $module_handler = \Drupal::service('module_handler');
    $module_path = $this->moduleHandler->getModule('webdebug')->getPath();

    $params = [
      'module_path' => $module_path,
      'port' => $this->port,
    ];

    $output = $this->twig
      ->loadTemplate('webdebug-client.html.twig')
      ->render($params);

    $response = new Response();
    $response
      ->setContent($output)
      ->setStatusCode(200)
      ->send();
    exit();
  }

}
