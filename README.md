# Webdebug

## About

This module provides two main features.

1. An API to let developers send messages and dumps to a debug client, and to halt execution by setting breakpoints.
2. A web page for error reporting, to replace Drupal's standard White Screen of Death.

## Warning

This module is a DEVELOPMENT tool and should only be used in DEVELOPMENT environments.

Debugging tools show as much contextual information as possible to help you debug. It is likely that this contextual information contains sensitive data such as API keys and database connection details. This module could potentially expose this information to end users. 

This can be a serious **security risk**. Do not install this module on production environments.

## Background

### Dump debugging versus step debugging

### Dump debugging
Dump debugging is the practice of interrupting code execution using debug statements like `print_r($var)`, `var_dump($var)`, and `die('some message here')` so you can inspect variables and see what's going on.

### Step debugging
Step debugging is based on tools like [Xdebug](https://xdebug.org) that integrate with your IDE, let you put breakpoints inside your code, and then let you step through the code line by line, inspecting variables along the way.

### Browser based dump debugging

This module fits somewhere in between:
* place `message()`, `dump()` or `breakpoint()` statements throughout your code.
* see the output appear in a terminal window or separate browser tab in real time (no reloading).

### Examples

```php
\Drupal::service('webdebug')->message('before DB connection');
```

```php
\Drupal::service('webdebug')->dump($node);
```

```php
\Drupal::service('webdebug')->breakpoint();
```

### Why the need for this module?

Not everyone is able or willing to set up and use xDebug for step debugging.

Standard dump debugging messages are usually rendered as part of your page's HTML output. This can cause all sorts of problems such as:

* debug output being hard to read due to theming/styling issues
* debug output lost if PHP's output buffer is flushed before the debug output had a chance to be rendered
* PHP running out of memory when you try to `print_r` large and complex objects like entities with entity references 
* `Warning: Cannot modify header information - headers already sent'


### A web page for error handling
This module intercepts fatal errors and shows them on a useful web page, rather than the standard White Screen of Death Drupal produces.

The error page:
* includes a full stack trace
* shows the relevant code surrounding each stack frame
* shows the environment variables and request/response objects
* lets you inspect and drill down into class source code 
  (requires the [Introspector](https://drupal.org/project/introspector)) module.

## Installation

 Install with composer:

 ```
 composer require drupal/webdebug
 ```

 ## Configuration

The Webdebug server listens to `localhost` on port `9977` by default.

You can change the port number by setting `$settings['debug_webport']` in your `settings.php`:

```php
// Set webdebug port to 9999.
$settings['webdebug_port'] = '9999';
```

### Port mapping
If you're working in a dockerized container environment (such as DDEV or Lando), or a virtual machine environment (such as Vagrant/Virtualbox or DrupalVM), you will need to map the webdebug port from your host environment to your virtual/container environment, otherwise the Webdebug browser client (running in the browser on your host machine) will not be able to communicate with the Webdebug server (running inside the containerized/virtual environment).

If you're unsure how to do this, please refer to the _Port mapping_ section of this document.

## Usage

### Webdebug server and client(s)

The webdebug server's job is to facilitate communication between your site (where you've embedded your debugging statements), and one or more debug clients (terminal or web page) that show the output of your debugging statements.

If you don't have a running debug server, the output of your debug statements will be sent nowhere.

#### 1. Start the webdebug server in your terminal

```
drush webdebug:server
```

or 

```
drush wdbs
```

The webdebug server currently also acts as a client, so all your debug output will be shown in the terminal window where you started the server. In the future you may need to start a separate debug client if you want to see debug output in a terminal window.

##### Example output

```
---------------
09:27:42 | 15295567947641 | inside the controller
>> /modules/custom/my_module/src/Controller/HelloController.php (line 30)


New connection! (1520)

---------------
09:27:42 | 15295721655196 | dump comment author
>> /modules/custom/my_route_demo/src/Controller/HelloController.php (line 31)

Drupal\Core\Session\AccountProxy {#194
  #account: Drupal\Core\Session\UserSession {#241
    #uid: "1"
    #roles: [
      "authenticated"
      "administrator"
    ]
    #access: "1627205215"
    +name: "admin"
    #preferred_langcode: "en"
    #preferred_admin_langcode: null
    #mail: "admin@example.com"
    #timezone: "UTC"
    +"langcode": "en"
    +"pass": "$S$EKDuNtIQxgFbXC3v6jC/.XXXXXXXXXXXXXXX.KEI9EcK"
    +"status": "1"
    +"created": "1606046785"
    +"changed": "1606046874"
    +"login": "1627051659"
    +"init": "admin@example.com"
    +"default_langcode": "1"
  }
  #id: "1"
  #initialAccountId: null
  #eventDispatcher: Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher {#61 …4}
  #_serviceIds: []
  #_entityStorages: []
  +"_serviceId": "current_user"
}

```

#### 2. Load the webdebug browser client
To see the debug output in a richer environment and drill down into all of the contextual information, open a separate browser tab or window and load `/admin/webdebug/client`.

The Webdebug server sends information to Webdebug browser clients in real-time, so you don't need to reload the browser client to see new debug output.

Reloading the browser client clears all of your debug output.

### Webdebug API

#### Message()

Sends a text string to Webdebug clients.

##### Examples

```php
\Drupal::service('webdebug')->message("inside the block's build() method");
```

```php
\Drupal::service('webdebug')->message('user id:' . $user->id());
```

```php
// Show the name of the current route.
$route_name = \Drupal::routeMatch()->getRouteName();
\Drupal::service('webdebug')->message($route_name);
```

#### Dump()

Sends a variable to Webdebug clients, where it is dumped (recursively).

##### Examples

```php
// Inspect the current user.
$user = \Drupal::currentUser();
\Drupal::service('webdebug')->dump($user);
```

```php
// Inspect the current node.
$node = \Drupal::routeMatch()->getParameter('node');
\Drupal::service('webdebug')->dump($node);
```

### Breakpoint()

Halts all code execution until you click `release breakpoint` in the Webdebug browser client.

#### Limitations
For now you can only halt execution and resume execution.

We are working on adding interactivity so you can inspect any variable or evaluate arbitrary statements while execution is halted (like you would do in the _Immediate_ window of a step debugger).

Note: for now you can only resume execution via _browser_ clients. We are working on an interactive terminal client.


### Error page
No setup required.  
Any error that would generate Drupal's white screen with 
the message `The website encountered an unexpected error. Please try again 
later.` should now show Webdebug's full error page instead.

### Known issues
* A tiny core patch is necessary for Webdebug to catch fatal errors (Drupal currently doesn't let you intercept these graciously).
* This module cannot catch `server 500` errors that are generated outside of Drupal, such as webserver misconfigurations. These kinds of errors will still produce a White Screen of Death, or whatever kind of error handling mechanism you may have set up on web server level.

## Credits

This module was inspired by [Ray](https://myray.app/), a dump debugging tool built by the brilliant folks at [Spatie](https://spatie.be/).

Part of this module's architecture and approach is based on Mozilla's [Remote Debugging Protocol](https://firefox-source-docs.mozilla.org/devtools/backend/protocol.html).

This module could not have existed without:
* [ratchetphp/ratchet](https://github.com/ratchetphp/Ratchet), a PHP library for asynchronously serving WebSockets.
* [ratchetphp/pawl](https://github.com/ratchetphp/Pawl), an asynchronous WebSocket client in PHP.
* [roave/better-reflection](https://github.com/Roave/BetterReflection/), a rich PHP reflection API.
* [symfony/var-dumper](), a memory safe recursive dumper